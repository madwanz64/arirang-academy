<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');


Route::middleware('guest')->group(function () {

    Route::get('login', function () {
        return view('login');
    })->name('login');

    Route::post('login', [\App\Http\Controllers\LoginController::class, 'authenticate'])->name('login.authenticate');

});

Route::middleware('auth')->group(function () {
    Route::post('logout', [\App\Http\Controllers\LoginController::class, 'destroy'])->name('logout');
});

Route::get('categories/{slug}', [\App\Http\Controllers\CategoryController::class, 'show'])->name('article.index');

Route::get('articles/{article}', [\App\Http\Controllers\ArticleController::class, 'show'])->name('article.show');

Route::prefix('admin')->middleware('auth')->name('admin.')->group(function () {
    Route::middleware('author')->group(function () {
        Route::get('dashboard', [\App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');

        Route::prefix('articles')->name('article.')->group(function () {
            Route::get('', [\App\Http\Controllers\ArticleController::class, 'index'])->name('index');
            Route::get('create', [\App\Http\Controllers\ArticleController::class, 'create'])->name('create');
            Route::get('get-articles', [\App\Http\Controllers\ArticleController::class, 'getArticles'])->name('get-articles');
            Route::get('{article}/edit', [\App\Http\Controllers\ArticleController::class, 'edit'])->name('edit');
            Route::post('', [\App\Http\Controllers\ArticleController::class, 'store'])->name('store');
            Route::post('{article}/update', [\App\Http\Controllers\ArticleController::class, 'update'])->name('update');
            Route::post('{article}/destroy', [\App\Http\Controllers\ArticleController::class, 'destroy'])->name('destroy');
        });
    });

    Route::middleware('admin')->group(function () {
        Route::prefix('users')->name('user.')->group(function () {
            Route::get('', [\App\Http\Controllers\UserController::class, 'index'])->name('index');
            Route::get('create', [\App\Http\Controllers\UserController::class, 'create'])->name('create');
            Route::get('get-users', [\App\Http\Controllers\UserController::class, 'getUsers'])->name('get-users');
            Route::post('', [\App\Http\Controllers\UserController::class, 'store'])->name('store');
        });
    });
});

Route::prefix('profiles')->middleware(['auth', 'owner'])->name('profile.')->group(function () {
    Route::get('{user}', [\App\Http\Controllers\ProfileController::class, 'show'])->name('show');
    Route::post('{user}/update-profile-data', [\App\Http\Controllers\ProfileController::class, 'updateProfileData'])->name('update-profile-data');
    Route::post('{user}/update-profile-image', [\App\Http\Controllers\ProfileController::class, 'updateProfileImage'])->name('update-profile-image');
    Route::post('{user}/delete-profile-image', [\App\Http\Controllers\ProfileController::class, 'deleteProfileImage'])->name('delete-profile-image');
    Route::post('{user}/change-password', [\App\Http\Controllers\ProfileController::class, 'changePassword'])->name('change-password');
});
