@extends('app')

@section('content')
    <section class="clean-block clean-form dark section-bg">
        <div class="container">
            <div class="block-heading" style="color: white;">
                <h1><b>Login</b></h1>
                <h6><b>Halo yeorobun!</b></h6>
                <h6><b>Silahkan login untuk langsung meluncur ke halaman Arirang Korean Course</b></h6>
            </div>
            <form action="{{ route('login.authenticate') }}" method="POST">
                @csrf
{{--                <p>Belum Punya Akun? <b><a id="btn-login-page" href="register.html">Register</a></b></p>--}}
                <div class="mb-3">
                    <label class="form-label" for="email">Email</label>
                    <input class="form-control item" type="email" id="email" name="email">
                </div>
                <div class="mb-3">
                    <label class="form-label" for="password">Password</label>
                    <input class="form-control" type="password" id="password" name="password">
                </div>
                <div class="mb-3">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="checkbox">
                        <label class="form-check-label" for="checkbox">Remember me</label>
                    </div>
                </div>
                <button class="btn btn-primary" type="submit">Log In</button>
            </form>
        </div>
    </section>
@endsection
