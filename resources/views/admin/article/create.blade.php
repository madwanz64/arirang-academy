@extends('admin.layout')

@section('content')
    <div class="pagetitle">
        <h1>Artikel</h1>
    </div><!-- End Page Title -->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Menulis Artikel</h5>

            <!-- General Form Elements -->
            <form id="create-article-form">
                @csrf
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label" for="category_id">Kategori</label>
                    <div class="col-sm-10">
                        <select class="form-select" aria-label="Default select example" name="category_id" id="category_id">
                            <option value="" selected disabled>Silakan Pilih Kategori</option>
                            @foreach($categories as $parent)
                                <optgroup label="{{ $parent->name }}">
                                    @foreach($parent->children as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </optgroup>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="cover_image" class="col-sm-2 col-form-label">Gambar Sampul</label>
                    <div class="col-sm-10">
                        <input class="form-control" type="file" id="cover_image" name="cover_image">
                        <div id="imagePreview" class="mt-3 d-none border" style="max-width: 400px"></div>
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="title" class="col-sm-2 col-form-label">Judul Artikel</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="content" class="col-sm-2 col-form-label">Isi Artikel</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" rows="12" id="content" name="content"></textarea>
                    </div>
                </div>

                <fieldset class="row mb-3">
                    <legend class="col-form-label col-sm-2 pt-0">Artikel</legend>
                    <div class="col-sm-10">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_locked" id="is_locked1" value="1" checked>
                            <label class="form-check-label" for="is_locked1">
                                Lock (hanya untuk member)
                            </label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="is_locked" id="is_locked2" value="0">
                            <label class="form-check-label" for="is_locked2">
                                Unlock (siapapun bisa membacanya)
                            </label>
                        </div>
                    </div>
                </fieldset>

                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label">Submit Button</label>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit Form</button>
                    </div>
                </div>

            </form><!-- End General Form Elements -->

        </div>
    </div>
@endsection

@push('customjs')
    <script>
        ClassicEditor
            .create( document.querySelector('#content'), {
                toolbar: [ 'undo', 'redo', 'bold', 'italic', 'numberedList', 'bulletedList' ]
            })

        function previewImage(event) {
            var imageInput = event.target;
            var imagePreview = document.getElementById('imagePreview');

            if (imageInput.files && imageInput.files[0]) {
                var reader = new FileReader();


                reader.onload = function (e) {
                    imagePreview.classList.remove('d-none')
                    imagePreview.innerHTML = '<img alt="Image Preview" class="img-fluid" src="' + e.target.result + '">';
                };

                reader.readAsDataURL(imageInput.files[0]);
            } else {
                imagePreview.innerHTML = '';
            }
        }

        $('#create-article-form [name="cover_image"]').on('change', function (e) {
            previewImage(e);
        })

        $('#create-article-form').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                url : '{{ route('admin.article.store') }}',
                type : 'POST',
                data : new FormData(this),
                processData : false,
                contentType : false,
                headers : {
                    'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                },
                success : function (response) {
                    Swal.fire({
                        icon : 'success',
                        title : 'Success',
                        text : response.message,
                        timer : 3000,
                        timerProgressBar: true,
                        onAfterClose : function () {
                            location.reload();
                        }
                    })
                },
                error : function (xhr, status, error) {
                    if (xhr.status === 422) {
                        let errors = xhr.responseJSON.errors;
                        $('#create-article-form :input').each(function () {
                            $(this).parent().children('div.invalid-feedback').remove();
                            $(this).removeClass('is-invalid');
                            if (errors.hasOwnProperty($(this).attr('name'))) {
                                let errorMessage = document.createElement('div');
                                errorMessage.classList.add('invalid-feedback');
                                errorMessage.innerHTML = errors[$(this).attr('name')];
                                $(this).parent().append(errorMessage);
                                $(this).addClass('is-invalid');
                            }
                        })
                    }
                    Toast.fire({
                        icon : "error",
                        title : error
                    })
                }
            })
        })
    </script>
@endpush
