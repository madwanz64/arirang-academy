@extends('admin.layout')

@section('content')
    <div class="pagetitle">
        <h1>Artikel</h1>
    </div><!-- End Page Title -->
    <div class="card">
        <div class="card-body" style="overflow-x: scroll">
            <h5 class="card-title">Daftar Artikel</h5>
            <table id="articles-data-table" class="table table-striped">
                <thead>
                <tr>
                    <th>Judul Artikel</th>
                    <th>Kategori</th>
                    <th>Bebas Akses</th>
                    <th>Penulis</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Judul Artikel</th>
                    <th>Kategori</th>
                    <th>Bebas Akses</th>
                    <th>Penulis</th>
                    <th>Action</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@push('customjs')
    <script>
        $(document).ready(function () {
            let table = $('#articles-data-table').DataTable({
                processing : true,
                serverSide : true,
                ajax : {
                    url : '{{ route('admin.article.get-articles') }}',
                    type : 'GET'
                },
                columns : [
                    {
                        data : 'title'
                    },
                    {
                        data : 'name'
                    },
                    {
                        data : 'is_locked',
                        orderable : false,
                        render : function (t) {
                            return t == 1 ? "Semua" : "Hanya Member"
                        }
                    },
                    {
                        data : 'author'
                    },
                    {
                        class : 'text-center',
                        data : 'id',
                        orderable: false,
                        defaultContent : '',
                        render : function (t) {
                            let edit = `<a class="btn btn-outline-secondary" href="{{ route('admin.article.edit', ['article' => '%%id%%']) }}"><i class="bi bi-pencil-square"></i></a>`
                            edit = edit.replace('%%id%%', t)
                            return edit;
                        }
                    }
                ]
            })
        })
    </script>
@endpush
