<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Dashboard Admin</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/boxicons/css/boxicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/quill/quill.snow.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/quill/quill.bubble.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/remixicon/remixicon.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/simple-datatables/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.13.4/css/jquery.dataTables.css" />
    <!-- CropperJS -->
    <link rel="stylesheet" href="{{asset('assets/css/cropper.min.css')}}">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/style-admin.css') }}" rel="stylesheet">
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
        <a href="{{ route('home') }}" class="logo d-flex align-items-center">
            <span class="d-none d-lg-block">Arirang Academy</span>
        </a>
        <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo -->

    <nav class="header-nav ms-auto">
        <ul class="d-flex align-items-center">

            <li class="nav-item dropdown pe-3">

                <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
                    @if(Auth::user()->profile_image != null && File::exists(public_path(Auth::user()->profile_image)))
                        <img src="{{ asset(Auth::user()->profile_image) }}" alt="Profile" class="rounded-circle">
                    @else
                        <img src="https://ui-avatars.com/api/?name={{ Auth::user()->name }}" alt="Profile" class="rounded-circle">
                    @endif
                    <span class="d-none d-md-block dropdown-toggle ps-2">{{ Auth::user()->name }}</span>
                </a><!-- End Profile Iamge Icon -->

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                    <li class="dropdown-header">
                        <h6>{{ Auth::user()->name }}</h6>
                        <span>{{ ucwords(strtolower(Auth::user()->role)) }}</span>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <a class="dropdown-item d-flex align-items-center" href="{{ route('profile.show', ['user' => Auth::id()]) }}">
                            <i class="bi bi-gear"></i>
                            <span>Account Settings</span>
                        </a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li id="sign-out-button">
                        <form action="{{ route('logout') }}" method="POST" id="logout">
                            @csrf
                            <a class="dropdown-item d-flex align-items-center" href="#">
                                <i class="bi bi-box-arrow-right"></i>
                                <span >Sign Out</span>
                            </a>
                        </form>
                    </li>

                </ul><!-- End Profile Dropdown Items -->
            </li><!-- End Profile Nav -->

        </ul>
    </nav><!-- End Icons Navigation -->

</header><!-- End Header -->

<!-- ======= Sidebar ======= -->
<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">
        @if(Auth::user()->role == 'PENULIS' || Auth::user()->role == 'ADMIN')

            <li class="nav-item ">
                <a class="nav-link {{ str_contains(Route::currentRouteName(), 'admin.dashboard') ? 'active' : "" }}" href="{{ route('admin.dashboard') }}">
                    <i class="bi bi-grid"></i>
                    <span>Dashboard</span>
                </a>
            </li><!-- End Dashboard Nav -->

            <li class="nav-item">
                <a class="nav-link {{ str_contains(Route::currentRouteName(), 'admin.article.') ? 'active' : "collapsed" }}" data-bs-target="#articles-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Artikel</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="articles-nav" class="nav-content collapse {{ str_contains(Route::currentRouteName(), 'admin.article.') ? 'show' : "" }}" data-bs-parent="#sidebar-nav">
                    <li>
                        <a class="nav-link {{ str_contains(Route::currentRouteName(), 'admin.article.index') ? 'active' : "" }}" href="{{ route('admin.article.index') }}">
                            <i class="bi bi-circle"></i><span>Daftar Artikel</span>
                        </a>
                    </li>
                    <li>
                        <a class="nav-link {{ str_contains(Route::currentRouteName(), 'admin.article.create') ? 'active' : "" }}" href="{{ route('admin.article.create') }}">
                            <i class="bi bi-circle"></i><span>Menulis Artikel</span>
                        </a>
                    </li>
                </ul>
            </li><!-- End Artikel Nav -->

            @if(Auth::user()->role == 'ADMIN')
                <li class="nav-item">
                    <a class="nav-link {{ str_contains(Route::currentRouteName(), 'admin.user.') ? 'active' : "collapsed" }}" data-bs-target="#users-nav" data-bs-toggle="collapse" href="#">
                        <i class="bi bi-people"></i><span>Pengguna</span><i class="bi bi-chevron-down ms-auto"></i>
                    </a>
                    <ul id="users-nav" class="nav-content collapse {{ str_contains(Route::currentRouteName(), 'admin.user.') ? 'show' : "" }}" data-bs-parent="#sidebar-nav">
                        <li>
                            <a class="nav-link {{ str_contains(Route::currentRouteName(), 'admin.user.index') ? 'active' : "" }}" href="{{ route('admin.user.index') }}">
                                <i class="bi bi-circle"></i><span>Daftar Pengguna</span>
                            </a>
                        </li>
                        <li>
                            <a class="nav-link {{ str_contains(Route::currentRouteName(), 'admin.user.create') ? 'active' : "" }}" href="{{ route('admin.user.create') }}">
                                <i class="bi bi-circle"></i><span>Tambah Pengguna</span>
                            </a>
                        </li>
                    </ul>
                </li><!-- End Artikel Nav -->
            @endif
        @endif

        <li class="nav-heading">Pages</li>

        <li class="nav-item">
            <a class="nav-link {{ str_contains(Route::currentRouteName(), 'profile') ? 'active' : "" }}" href="{{ route('profile.show', ['user' => Auth::id()]) }}">
                <i class="bi bi-person"></i>
                <span>Profile</span>
            </a>
        </li><!-- End Profile Page Nav -->

    </ul>

</aside><!-- End Sidebar-->

<main id="main" class="main">

    @yield('content')

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer" class="footer">
    <div class="copyright">
        &copy; Copyright <strong><span>Arirang Academy</span></strong>. 2023
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<script src="https://code.jquery.com/jquery-3.7.0.min.js" integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>

<!-- Vendor JS Files -->
<script src="{{ asset('assets/vendor/apexcharts/apexcharts.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/chart.js/chart.umd.js') }}"></script>
<script src="{{ asset('assets/vendor/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('assets/vendor/quill/quill.min.js') }}"></script>
<script src="{{ asset('assets/vendor/simple-datatables/simple-datatables.js') }}"></script>
<script src="{{ asset('assets/vendor/tinymce/tinymce.min.js') }}"></script>
<script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assets/js/main-admin.js') }}"></script>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>
    const Toast = Swal.mixin({
        toast: true,
        position: 'bottom-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
    })
</script>
<script src="https://cdn.datatables.net/1.13.4/js/jquery.dataTables.js"></script>
<script src="https://cdn.ckeditor.com/ckeditor5/38.0.1/classic/ckeditor.js"></script>
<!-- CropperJS -->
<script src="{{asset('assets/js/cropper.min.js')}}"></script>

<script>
    $(document).ready(function () {
        $('#sign-out-button').on('click', function (e) {
            e.preventDefault()
            $('form#logout').submit();
        })
    })
</script>

@stack('customjs')

</body>

</html>
