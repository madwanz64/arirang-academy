@extends('admin.layout')

@section('content')
    <div class="pagetitle">
        <h1>Pengguna</h1>
    </div><!-- End Page Title -->
    <div class="card">
        <div class="card-body">
            <h5 class="card-title">Tambah Pengguna</h5>

            <!-- General Form Elements -->
            <form id="create-user-form">
                @csrf
                <div class="row mb-3">
                    <label for="name" class="col-sm-2 col-form-label">Nama Pengguna</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="email" class="col-sm-2 col-form-label">Email Pengguna</label>
                    <div class="col-sm-10">
                        <input type="email" class="form-control" id="email" name="email">
                    </div>
                </div>

                <div class="row mb-3">
                    <label for="password" class="col-sm-2 col-form-label">Kata Sandi</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <button class="btn btn-outline-secondary" type="button" id="generate-password">Generate</button>
                            <input type="text" class="form-control" id="password" name="password">
                        </div>
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label" for="role">Role</label>
                    <div class="col-sm-10">
                        <select class="form-select" aria-label="Default select example" name="role" id="role">
                            @foreach($roles as $role)
                                <option value="{{ $role }}" {{ $role == 'MEMBER' ? 'selected' : '' }}>{{ $role }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label">Action Button</label>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit Form</button>
                    </div>
                </div>

            </form><!-- End General Form Elements -->

        </div>
    </div>
@endsection

@push('customjs')
    <script>
        function generatePassword() {
            var length = 12; // Length of the generated password
            var charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-+="; // Characters to include in the password
            var password = "";

            for (var i = 0, n = charset.length; i < length; ++i) {
                password += charset.charAt(Math.floor(Math.random() * n));
            }

            document.getElementById("password").value = password;
        }

        $('#generate-password').on('click', function () {
            generatePassword()
        })

        $('#create-user-form').on('submit', function (e) {
            e.preventDefault();

            $.ajax({
                url : '{{ route('admin.user.store') }}',
                type : 'POST',
                data : new FormData(this),
                processData : false,
                contentType : false,
                headers : {
                    'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                },
                success : function (response) {
                    $('#create-user-form').trigger('reset');
                    $('#create-user-form :input').each(function () {
                        $(this).parent().children('div.invalid-feedback').remove();
                        $(this).removeClass('is-invalid');
                    })
                    Swal.fire({
                        icon : 'success',
                        title : 'Success',
                        text : response.message,
                        timer : 3000,
                        timerProgressBar: true
                    })
                },
                error : function (xhr, status, error) {
                    if (xhr.status === 422) {
                        let errors = xhr.responseJSON.errors;
                        $('#create-user-form :input').each(function () {
                            $(this).parent().children('div.invalid-feedback').remove();
                            $(this).removeClass('is-invalid');
                            if (errors.hasOwnProperty($(this).attr('name'))) {
                                let errorMessage = document.createElement('div');
                                errorMessage.classList.add('invalid-feedback');
                                errorMessage.innerHTML = errors[$(this).attr('name')];
                                $(this).parent().append(errorMessage);
                                $(this).addClass('is-invalid');
                            }
                        })
                    }
                    Toast.fire({
                        icon : "error",
                        title : error
                    })
                }
            })
        })
    </script>
@endpush
