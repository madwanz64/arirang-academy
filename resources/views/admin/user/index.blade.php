@extends('admin.layout')

@section('content')
    <div class="pagetitle">
        <h1>Pengguna</h1>
    </div><!-- End Page Title -->
    <div class="card">
        <div class="card-body" style="overflow-x: scroll">
            <h5 class="card-title">Daftar Pengguna</h5>
            <table id="users-data-table" class="table table-striped">
                <thead>
                <tr>
                    <th>Nama Pengguna</th>
                    <th>Email</th>
                    <th>Role User</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tfoot>
                <tr>
                    <th>Nama Pengguna</th>
                    <th>Email</th>
                    <th>Role User</th>
                    <th>Action</th>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection

@push('customjs')
    <script>
        $(document).ready(function () {
            let table = $('#users-data-table').DataTable({
                processing : true,
                serverSide : true,
                ajax : {
                    url : '{{ route('admin.user.get-users') }}',
                    type : 'GET'
                },
                columns : [
                    {
                        data : 'name'
                    },
                    {
                        data : 'email'
                    },
                    {
                        data : 'role'
                    },
                    {
                        class : 'text-center',
                        data : 'id',
                        orderable: false,
                        defaultContent : '',
                        render : function (t) {
                            let edit = `<a class="btn btn-outline-secondary" href="{{ route('profile.show', ['user' => '%%id%%']) }}"><i class="bi bi-pencil-square"></i></a>`
                            edit = edit.replace('%%id%%', t)
                            return edit;
                        }
                    }
                ]
            })
        })
    </script>
@endpush
