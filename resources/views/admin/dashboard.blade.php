@extends('admin.layout')

@section('content')
    <div class="pagetitle">
        <h1>Dashboard</h1>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row">
            <div class="col-xxl-4 col-md-6">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="card-title">Jumlah Member</h5>
                        <div class="d-flex align-items-center">
                            <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                <i class="bi bi-people"></i>
                            </div>
                            <div class="ps-3">
                                <h6>{{ $memberCount }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4 col-md-6">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="card-title">Jumlah Penulis</h5>
                        <div class="d-flex align-items-center">
                            <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                <i class="bi bi-person-gear"></i>
                            </div>
                            <div class="ps-3">
                                <h6>{{ $authorCount }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xxl-4 col-md-6">
                <div class="card info-card sales-card">
                    <div class="card-body">
                        <h5 class="card-title">Jumlah Artikel</h5>
                        <div class="d-flex align-items-center">
                            <div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
                                <i class="bi bi-files"></i>
                            </div>
                            <div class="ps-3">
                                <h6>{{ $articlesCount }}</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col ">
                <!-- News & Updates Traffic -->
                <div class="card">
                    <div class="card-body pb-0">
                        <h5 class="card-title">Artikel Terbaru</h5>

                        <div class="news">
                            @foreach($recentArticles as $article)
                                <div class="post-item clearfix">
                                    <img src="{{ asset($article->cover_image) }}" alt="">
                                    <h4><a href="{{ route('article.show', ['article' => $article->id]) }}">{{ $article->title }} <span class="text-muted">({{ \Illuminate\Support\Carbon::parse($article->created_at)->toFormattedDateString() }})</span></a></h4>
                                    <div class="row mb-3">
                                        <div>
                                            <a href="{{ route('admin.article.edit', ['article' => $article->id]) }}" class="btn btn-sm btn-outline-secondary"><i class="bi bi-pencil-square"></i> Edit</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div><!-- End sidebar recent posts-->
                    </div>
                </div><!-- End News & Updates -->
            </div>


        </div>
    </section>
@endsection
