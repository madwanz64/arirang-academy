@extends('app')

@section('banner')

@endsection

@section('content')
    <!-- ======= Home Section ======= -->
    <section id="home" class="home d-flex align-items-center section-bg">
        <div class="container">
            <div class="row justify-content-between gy-5">
                <div
                    class="col-lg-5 order-2 order-lg-1 d-flex flex-column justify-content-center align-items-center align-items-lg-start text-center text-lg-start">
                    <h1 data-aos="fade-up">Pendidikan Bahasa Korea,<br>Arirang Academy</h1>
                    <p data-aos="fade-up" data-aos-delay="100"><u>PT Arirang Pesona Budaya Korea.</u></p>
                    <div class="d-flex" data-aos="fade-up" data-aos-delay="200">
                        <a href="#greeting" class="btn-book-a-table">Informasi Lebih Lanjut</a>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- End Home Section -->
    <section id="greeting" class="greeting">
        <div style="padding: 30px;">
            <div class="container">
                <div class="row" style="margin-top: 30px;">
                    <div
                        class="col-md-12 text-start text-sm-start text-md-start text-lg-start text-xl-center text-xxl-center">
                        <strong
                            class="text-center text-sm-center text-md-center text-lg-center text-xl-center text-xxl-center"
                            style="font-size: 30px;color: var(--bs-blue);"
                            style="margin-top: 120px;">Greeting</strong>
                        <p><strong><span style="color: rgb(68, 68, 68);">안녕하세요</span></strong><br></p>
                        <h1 class="text-center">Hallo Semuanya</h1>
                    </div>
                    <div class="col">
                        <p style="text-align: justify;"><span style="color: rgb(68, 68, 68);">우리 아리랑 한국어학원을 여러분들에게
                                    소개하게 되어 매우 기쁩니다. 저는 아리랑 한국어 학원장 James Park입니다. 인도네시아와 한국간에 경제적 문화적 교류가 깊어지는 가운데 아리랑
                                    한국어 학원을 문화의 도시 반둥에 설립하게 되어 그 의미가 더욱 크다고 하겠습니다. 우리 학원은 인도네시아인들의 한국어 습득열기에 대응하여 한국문화를
                                    더 잘 이해하고 한국어를 보다 더 잘 구사할 수 있도록 최선을 다하여 돕겠습니다.우리 학원은 엄선된 탁월한 강사분들이 가장 재미있고 가장 효율적인
                                    교수법으로 강의를 할 것입니다. 유능한 인도네시아인 강사는 물론 한국 원어민 강사도 초빙하였습니다. 또한 인터넷 Zoom 시스템을 통하여 한국에 있는
                                    한국인 강사와 On Line 수업도 할 수 있습니다. 수강생들은 선호에 따라 Regular반(10~20명), Privare반(5~7명),
                                    VIP반(1~2명)을 선택할 수 있고 한국어 수준에 따라 반이 배정될 것입니다. 다양한 시청각 자료를 준비하고 있으며 수강생들이 원할 경우 한국에 있는
                                    개인적 Network 을 활용하여 한국유학과 취업에 관한 안내도 받을 수 있습니다.우리 아리랑 학원을 통하여 여러분들이 원하는 소기의 목적을 달성하여
                                    만족스런 결과를 얻기를 바라며 이후에도 계속하여 우리 학원에 관심을 갖고 또 주변의 많은 사람들에게 알리어 한국과 인도네시아간에 가교역할을 할 수
                                    있기를 기대합니다 감사합니다</span><br></p>
                        <p style="text-align: justify;"><br><span style="color: rgb(68, 68, 68);">Saya sangat senang
                                    karena dapat memperkenalkan tempat Kursus bahasa Korea Arirang. Saya adalah James
                                    Park, kepala kursus bahasa Korea Arirang. Di tengah pertukaran ekonomi dan budaya
                                    antara Indonesia dan Korea yang semakin dalam, kursus bahasa Korea Arirang didirikan
                                    di Bandung yang merupakan kota berbudaya sehingga pendirian kursus bahasa Korea
                                    Arirang ini menjadi lebih bermakna. Kami akan melakukan yang terbaik untuk membantu
                                    orang Indonesia memahami budaya Korea dengan lebih baik dan berbicara bahasa Korea
                                    dengan lebih baik sebagai tanggapan atas antusiasme belajar bahasa Korea.Kelas di
                                    tempat kursus kami akan dilakukan dengan cara yang paling menyenangkan dan efisien
                                    oleh para pengajar unggul yang telah kami seleksi. Kami tentunya telah mengundang
                                    pengajar orang Indonesia yang kompeten juga pengajar asli orang Korea. Selain itu,
                                    kelas dapat dilakukan dengan pengajar orang Korea langsung secara daring melalui
                                    zoom. Kelas dibagi berdasarkan standar bahasa Korea dan para siswa dapat memilih
                                    tiga pilihan kelas, yaitu kelas reguler (10-20 orang), kelas privat (5-7 orang) dan
                                    kelas VIP (1-2 orang). Kami sedang mempersiapkan beragam bahan ajar audiovisual dan
                                    jika berminat, para siswa juga dapat menerima informasi mengenai dunia kerja dan
                                    dunia pendidikan Korea melalui jaringan pribadi yang kami miliki.Melalui Kursus
                                    Arirang kami, saya berharap Anda semua dapat mencapai tujuan yang diinginkan dan
                                    mendapatkan hasil yang memuaskan. Saya juga berharap Anda akan terus tertarik dengan
                                    tempat kursus kami dan dapat memberi tahu lebih banyak orang di sekitar Anda
                                    sehingga jembatan antara Korea dan Indonesia dapat
                                    terwujud.Terimakasih.</span><br><br><br></p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about" class="about">
        <div style="padding: 30px;">
            <div class="container">
                <div class="row" style="margin-top: 30px;">
                    <div
                        class="col-md-12 text-start text-sm-start text-md-start text-lg-start text-xl-center text-xxl-center">
                        <strong
                            class="text-center text-sm-center text-md-center text-lg-center text-xl-center text-xxl-center"
                            style="font-size: 30px;color: var(--bs-blue);" style="margin-top: 120px;">About</strong>
                        <h1 class="text-center">Latar Belakang</h1>
                    </div>
                    <div class="col">
                        <p style="text-align: justify;"><br><span style="color: rgb(68, 68, 68);">Bahasa Korea
                                    memiliki berbagai keunggulan untuk mampu berkembang menjadi bahasa komunikasi yang
                                    dapat menunjang karir dan studi masyarakat dunia, khususnya Indonesia yang memiliki
                                    relevansi kuat dengan Korea. PT Pendidikan Bahasa Korea merupakan salah satu lembaga
                                    pendidikan untuk membantu orang-orang Indonesia yang ingin mempelajari bahasa dan
                                    budaya Korea dalam meningkatkan sumber daya manusia Indonesia yang dikaitkan dengan
                                    program pengembangan sistem pendidikan dan pelatihan. Kebutuhan bahasa Korea sangat
                                    penting mengingat tantangan dan persaingan global pasar tenaga kerja nasional maupun
                                    internasional semakin terbuka. Pergerakan tenaga kerja antara Korea dan Indonesia
                                    tidak dapat dibendung dengan peraturan atau regulasi yang bersifat
                                    protektif.</span><br><br><br></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div
                    class="col-md-12 text-start text-sm-start text-md-start text-lg-start text-xl-center text-xxl-center">
                    <strong
                        class="text-center text-sm-center text-md-center text-lg-center text-xl-center text-xxl-center"
                        style="font-size: 30px;color: var(--bs-blue);">VISI & MISI</strong>
                    <h1 class="text-center">Pendidikan dan Pelatihan Bahasa Korea Arirang Academy</h1>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <h3 style="text-align: center;color: rgb(2,110,252);">Visi</h3>
                            <ul class="list-group">
                                <li class="list-group-item"><span>Membuka ruang budaya: pertukaran budaya Korea dan Indonesia<br><br></span></li>
                                <li class="list-group-item"><span>Perluasan pertukaran budaya dari kedua negara melalui buku cetakan atau forum diskusi.</span></li>
                                <li class="list-group-item"><span>Pengalaman budaya Korea melalui tontonan Hallyu<br><br></span></li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <h3 style="text-align: center;color: rgb(2,110,252);">Misi</h3>
                            <ul class="list-group">
                                <li class="list-group-item"><span>Belajar melalui pengalaman (pelatihan bahasa langsung bersama dengan penutur asli bahasa Korea)</span></li>
                                <li class="list-group-item"><span>Pengajar unggulan (Penutur asli pengajar bahasa Korea bersertifikat, orang Indonesia lulusan bahasa Korea, orang yang berpengalaman bekerja atau belajar di Korea)</span></li>
                                <li class="list-group-item"><span>Pendidikan menyesuaikan (kelas reguler, kelas privat dan kelas VIP)<br><br></span></li>
                            </ul>
                        </div>
                    </div>
                </div>
    </section>

    <section id="picture" class="picture">
        <div style="padding: 30px;">
            <div class="container">
                <div class="container py-4 py-xl-5">
                    <div class="row mb-5">
                        <div class="col-md-8 col-xl-6 text-center mx-auto">
                            <h2 style="font-size: 30px;color: var(--bs-blue);font-weight: bold;">PICTURE</h2>
                            <!-- <p class="w-lg-50">Teks</p> -->
                        </div>
                    </div>
                    <div class="row gy-4 row-cols-1 row-cols-md-2 row-cols-xl-3">
                        <div class="col">
                            <div><img class="rounded img-fluid d-block w-100 fit-cover" style="height: 250px;"
                                      src="/assets/img/picture/Tampilan depan.jpeg">
                                <div class="py-4">
                                    <h4 style="text-align: center;">Tampilan Depan</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div><img class="rounded img-fluid d-block w-100 fit-cover" style="height: 250px;"
                                      src="/assets/img/picture/Resepsionis.jpeg">
                                <div class="py-4">
                                    <h4 style="text-align: center;">Resepsionis</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div><img class="rounded img-fluid d-block w-100 fit-cover" style="height: 250px;"
                                      src="/assets/img/picture/kelas reguler.jpeg">
                                <div class="py-4">
                                    <h4 style="text-align: center;">Ruang Kelas Regular</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div><img class="rounded img-fluid d-block w-100 fit-cover" style="height: 250px;"
                                      src="/assets/img/picture/kelas reguler 2.jpeg">
                                <div class="py-4">
                                    <h4 style="text-align: center;">Ruang Kelas Reguler 2</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div><img class="rounded img-fluid d-block w-100 fit-cover" style="height: 250px;"
                                      src="/assets/img/picture/lt3.jpeg">
                                <div class="py-4">
                                    <h4 style="text-align: center;">Lantai 3</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div><img class="rounded img-fluid d-block w-100 fit-cover" style="height: 250px;"
                                      src="/assets/img/picture/lt3.jpeg">
                                <div class="py-4">
                                    <h4 style="text-align: center;">Lainnya</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </section>

    <section id="pengajar" id="pengajar">
        <div style="padding: 30px;">
            <div class="container py-4 py-xl-5">
                <div class="row mb-4 mb-lg-5">
                    <div class="col-md-8 col-xl-6 text-center mx-auto">
                        <h2 style="font-weight: bold;font-size: 30px;color: var(--bs-blue);">PENGAJAR</h2>
                        <!-- <p class="w-lg-50">Curae hendrerit donec commodo hendrerit egestas tempus, turpis facilisis nostra nunc. Vestibulum dui eget ultrices.</p> -->
                    </div>
                </div>
                <div class="row gy-4 row-cols-2 row-cols-md-4">
                    <div class="col">
                        <div class="card border-0 shadow-none">
                            <div class="card-body text-center d-flex flex-column align-items-center p-0"><img
                                    class="rounded-circle mb-3 fit-cover" width="130" height="130"
                                    src="/assets/img/pengajar/bpk.jpeg" style="width: 250px;height: 250px;">
                                <h5 class="fw-bold text-primary card-title mb-0"><strong>Mr.James
                                        (Direktur)</strong></h5>
                                <p class="text-muted card-text mb-2">Phd in Economics</p>
                                <p class="text-muted card-text mb-2">Qualification level 2 for Korean Languange
                                    Teacher</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card border-0 shadow-none">
                            <div class="card-body text-center d-flex flex-column align-items-center p-0"><img
                                    class="rounded-circle mb-3 fit-cover" width="130" height="130"
                                    src="/assets/img/pengajar/burhan.jpg" style="width: 250px;height: 250px;">
                                <h5 class="fw-bold text-primary card-title mb-0"><strong>Pak Burhan
                                        (Manager)</strong></h5>
                                <p class="text-muted card-text mb-2">Pengajar BIPA di Balai Bahasa UPI</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card border-0 shadow-none">
                            <div class="card-body text-center d-flex flex-column align-items-center p-0"><img
                                    class="rounded-circle mb-3 fit-cover" width="130" height="130"
                                    src="/assets/img/pengajar/Syifa Finance.jpeg" style="width: 250px;height: 250px;">
                                <h5 class="fw-bold text-primary card-title mb-0"><strong>Syifa (Finance)</strong>
                                </h5>
                                <p class="text-muted card-text mb-2">Lulusan Akomodasi Perhotelan</p>
                                <p class="text-muted card-text mb-2">Public Relation Event di Hotel & Restaurant
                                    Jakarta</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card border-0 shadow-none">
                            <div class="card-body text-center d-flex flex-column align-items-center p-0"><img
                                    class="rounded-circle mb-3 fit-cover" width="130" height="130"
                                    src="/assets/img/pengajar/Yang Syung Min (Ssaem).jpg"
                                    style="width: 250px;height: 250px;">
                                <h5 class="fw-bold text-primary card-title mb-0"><strong>Yang Syung Min
                                        (Ssaem)</strong></h5>
                                <p class="text-muted card-text mb-2">S2 Yeonsei University, Korea</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card border-0 shadow-none">
                            <div class="card-body text-center d-flex flex-column align-items-center p-0"><img
                                    class="rounded-circle mb-3 fit-cover" width="130" height="130"
                                    src="/assets/img/pengajar/Rere Ssaem edit .jpeg"
                                    style="width: 250px;height: 250px;">
                                <h5 class="fw-bold text-primary card-title mb-0"><strong>Rere (Ssaem)</strong></h5>
                                <p class="text-muted card-text mb-2">5 tahun sebagai Tutor Bahasa Korea, Pernah Ke
                                    Korea</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card border-0 shadow-none">
                            <div class="card-body text-center d-flex flex-column align-items-center p-0"><img
                                    class="rounded-circle mb-3 fit-cover" width="130" height="130"
                                    src="/assets/img/pengajar/Syifa Ssaem edit.jpeg"
                                    style="width: 250px;height: 250px;">
                                <h5 class="fw-bold text-primary card-title mb-0"><strong>Syifa (Ssaem)</strong></h5>
                                <p class="text-muted card-text mb-2">Pengajar BIPA di Balai Bahasa UPI</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card border-0 shadow-none">
                            <div class="card-body text-center d-flex flex-column align-items-center p-0"><img
                                    class="rounded-circle mb-3 fit-cover" width="130" height="130"
                                    src="/assets/img/pengajar/Arni Ssaem.jpeg" style="width: 250px;height: 250px;">
                                <h5 class="fw-bold text-primary card-title mb-0"><strong>Arni (Ssaem)</strong></h5>
                                <p class="text-muted card-text mb-2">Lulus Pendidikan Bahasa Korea UPI</p>
                            </div>
                        </div>
                    </div>

                    <div class="col">
                        <div class="card border-0 shadow-none">
                            <div class="card-body text-center d-flex flex-column align-items-center p-0"><img
                                    class="rounded-circle mb-3 fit-cover" width="130" height="130"
                                    src="/assets/img/pengajar/guru baru.png" style="width: 250px;height: 250px;">
                                <h5 class="fw-bold text-primary card-title mb-0"><strong>Nama Guru Baru</strong></h5>
                                <p class="text-muted card-text mb-2">Lulusan</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </section>

    <section id="program" class="program">
        <div style="padding: 30px;">
            <div class="container">
                <div class="row" style="margin-top: 30px;">
                    <div
                        class="col-md-12 text-start text-sm-start text-md-start text-lg-start text-xl-center text-xxl-center">
                        <strong
                            class="text-center text-sm-center text-md-center text-lg-center text-xl-center text-xxl-center"
                            style="font-size: 30px;color: var(--bs-blue);" style="margin-top: 120;">Program</strong>
                        <h1 class="text-center">Program Pendidikan dan Pelatihan Bahasa Korea </h1>
                    </div>
                    <div class="col">
                        <p style="text-align: justify;"><br><span style="color: rgb(68, 68, 68);">Program dan
                                    pelatihan kursus bahasa Korea Arirang merupakan program kursus dan pelatihan untuk
                                    menghasilkan lulusan yang terampil bahasa Korea baik lisan maupun tulisan untuk
                                    berbagai tujuan dan berbagai konteks yang sesuai dengan kompetensi masing-masing
                                    level. Dan menjadi sarana agar dapat bekerja di Korea, bekerja di perusahaan Korea
                                    yang ada di Indonesia.Kursus Bahasa Korea Arirang membuka pintu bagi siapapun yang
                                    ingin belajar. Mulai dari usia 7 tahun – dewasa ( dengan kelas terpisah sesuai
                                    kualifikasi umur) . Dengan metode pembelajaran Offline/Online, dengan tutor unggul
                                    yang sudah diseleksi berasal dari Indonesia maupun Korea. Dengan cara mengajar yang
                                    modern, membuat murid-murid tidak bosan dan mudah mengerti.Terdapat delapan level
                                    kompetensi yang di dalamnya meliputi empat kemahiran berbahasa, yaitu menyimak,
                                    berbicara, membaca dan menulis. Dari 8 level tersebut dibagi menjadi dua kategori
                                    yaitu chogeup dan junggeup. chogeup terdiri atas 4 level, chogeup 1, chogeup 2,
                                    chogeup 3, chogeup 4 dan junggeup terdiri atas 4 level juga, yaitu junggeup 1,
                                    junggeup 2, junggeup 3, junggeup 4.Adanya Ujian Kompetensi dalam setiap level agar
                                    bisa mengetahui sejauh mana perkembangan murid-murid selama belajar di Kursus Bahasa
                                    Korea Arirang. Dan ketika murid-murid lulus mereka mendapatkan Sertifikasi Kelulusan
                                    dari Kursus kami, mereka bisa membawa sertifikat itu menjadi sebuah bukti bahwa
                                    mereka sudah bisa berbahasa Korea lisan/tulisan dengan baik dan
                                    benar.</span><br><br><br></p>
                        <img src="/assets/img/harga les-1.png" style="border:0; width: 100%; height: 800px;"
                             alt="Harga Les" class="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="contact" class="contact">
        <div style="padding: 30px;">
            <div class="container">
                <div class="row" style="margin-top: 30px;">
                    <div class="col-md-8 col-xl-6 text-center mx-auto">
                        <h2 style="color: var(--bs-blue);font-weight: bold;font-size: 30px;">Contact Us</h2>
                    </div>
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-6 col-lg-4 col-xl-4">
                            <div class="d-flex flex-column justify-content-center align-items-start h-100">
                                <div class="d-flex align-items-center p-3">
                                    <div
                                        class="bs-icon-md bs-icon-rounded bs-icon-primary d-flex flex-shrink-0 justify-content-center align-items-center d-inline-block bs-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"
                                             fill="currentColor" viewBox="0 0 16 16" class="bi bi-telephone">
                                            <path
                                                d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z">
                                            </path>
                                        </svg></div>
                                    <div class="px-2">
                                        <h6 class="mb-0">Whatsapp</h6>
                                        <p class="mb-0">+62 812-1251-6593</p>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center p-3">
                                    <div
                                        class="bs-icon-md bs-icon-rounded bs-icon-primary d-flex flex-shrink-0 justify-content-center align-items-center d-inline-block bs-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"
                                             fill="currentColor" viewBox="0 0 16 16" class="bi bi-envelope">
                                            <path fill-rule="evenodd"
                                                  d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z">
                                            </path>
                                        </svg></div>
                                    <div class="px-2">
                                        <h6 class="mb-0">Email</h6>
                                        <p class="mb-0">arirangcourse.id@gmail.com</p>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center p-3">
                                    <div
                                        class="bs-icon-md bs-icon-rounded bs-icon-primary d-flex flex-shrink-0 justify-content-center align-items-center d-inline-block bs-icon">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em"
                                             fill="currentColor" viewBox="0 0 16 16" class="bi bi-pin">
                                            <path
                                                d="M4.146.146A.5.5 0 0 1 4.5 0h7a.5.5 0 0 1 .5.5c0 .68-.342 1.174-.646 1.479-.126.125-.25.224-.354.298v4.431l.078.048c.203.127.476.314.751.555C12.36 7.775 13 8.527 13 9.5a.5.5 0 0 1-.5.5h-4v4.5c0 .276-.224 1.5-.5 1.5s-.5-1.224-.5-1.5V10h-4a.5.5 0 0 1-.5-.5c0-.973.64-1.725 1.17-2.189A5.921 5.921 0 0 1 5 6.708V2.277a2.77 2.77 0 0 1-.354-.298C4.342 1.674 4 1.179 4 .5a.5.5 0 0 1 .146-.354zm1.58 1.408-.002-.001.002.001zm-.002-.001.002.001A.5.5 0 0 1 6 2v5a.5.5 0 0 1-.276.447h-.002l-.012.007-.054.03a4.922 4.922 0 0 0-.827.58c-.318.278-.585.596-.725.936h7.792c-.14-.34-.407-.658-.725-.936a4.915 4.915 0 0 0-.881-.61l-.012-.006h-.002A.5.5 0 0 1 10 7V2a.5.5 0 0 1 .295-.458 1.775 1.775 0 0 0 .351-.271c.08-.08.155-.17.214-.271H5.14c.06.1.133.191.214.271a1.78 1.78 0 0 0 .37.282z">
                                            </path>
                                        </svg></div>
                                    <div class="px-2">
                                        <h6 class="mb-0">Lokasi</h6>
                                        <p class="mb-0">Komplek Ruko A11, Jl. Terusan Sutami Jl. Setrawangi,
                                            Sukagalih, Megasquare, Kota Bandung, Jawa Barat 40163</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col"><iframe allowfullscreen="" frameborder="0"
                                                 src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3961.073277711127!2d107.58187491462553!3d-6.8818251950271!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e68e758a7750f7b%3A0xb7f6c69c30989dc3!2sArirang%20Korean%20Course!5e0!3m2!1sid!2sid!4v1654920267592!5m2!1sid!2sid"
                                                 width="100%" height="400"></iframe></div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
