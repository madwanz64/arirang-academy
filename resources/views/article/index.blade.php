@extends('app')

@section('content')
    <!-- Artikel -->
    <section>
        <div style="padding: 25px;">
            <div class="col-md-12 text-start text-sm-start text-md-start text-lg-start text-xl-center text-xxl-center">
                <strong
                    class="text-center text-sm-center text-md-center text-lg-center text-xl-center text-xxl-center"
                    style="font-size: 30px;color: var(--bs-blue);"
                    style="margin-top: 120px;">{{ $category->name }}</strong>
            </div>
            <div class="container py-4 py-xl-5">
                <div class="row gy-4 row-cols-1 row-cols-md-2 row-cols-xl-3 mb-3">
                    @foreach($articles as $article)
                        <div class="col">
                            <div class="card h-100">
                                <img class="card-img-top w-100 d-block fit-cover" style="height: 200px;" src="{{ asset($article->cover_image) }}">
                                <div class="card-body p-4 d-flex flex-column">
                                    <h4 class="card-title">{{ $article->title }}</h4>
                                    <p class="card-text" style="text-align: justify;">
                                        {{  strip_tags($article->content)  }} <a href="{{ route('article.show', ['article' => $article->id]) }}">Baca Selanjutnya</a>
                                    </p>
                                    <div class="d-flex mt-auto">
                                        @if($article->author->profile_image != null && File::exists(public_path($article->author->profile_image)))
                                            <img class="rounded-circle flex-shrink-0 me-3 fit-cover" width="50" height="50" src="{{ asset($article->author->profile_image) }}">
                                        @else
                                            <img class="rounded-circle flex-shrink-0 me-3 fit-cover" width="50" height="50" src="https://ui-avatars.com/api/?name={{ $article->author->name }}">
                                        @endif
                                        <div>
                                            <p class="fw-bold mb-0">{{ $article->author->name }}</p>
                                            <p class="text-muted mb-0">{{ \Illuminate\Support\Carbon::parse($article->created_at)->toFormattedDateString() }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                {{ $articles->links() }}
            </div>
        </div>
    </section>
    <!-- End Artikel -->

@endsection
