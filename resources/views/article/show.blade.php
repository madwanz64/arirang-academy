@extends('app')

@section('content')
    <section class="clean-block clean-post dark">
        <div class="container">
            <div class="block-content">
                <div class="card">
                    @if($article->cover_image != null && File::exists(public_path($article->cover_image)))
                        <img class="card-img-top w-100 d-block fit-cover" src="{{ asset($article->cover_image) }}">
                    @endif
                    <div class="post-body">
                        <h3>{{ $article->title }}</h3>
                        <div class="post-info"><span>By {{ $article->user->name }}</span><span>{{ \Illuminate\Support\Carbon::parse($article->created_at)->toFormattedDateString() }}</span></div>
                        {!! htmlspecialchars_decode($article->content) !!}
                        @if($article->is_locked === 1)
                            <section class="py-4 py-xl-5">
                                <div class="container">
                                    <div class="text-white bg-primary border rounded border-0 border-primary d-flex flex-column justify-content-between flex-lg-row p-4 p-md-5">
                                        <div class="pb-2 pb-lg-1">
                                            <h2 class="fw-bold mb-2">Subscribe!</h2>
                                            <p class="mb-0">Untuk mengakses lebih banyak informasi, kalian bisa menjadi member AKC!</p>
                                        </div>
                                        <div class="my-2"><a class="btn btn-light fs-5 py-2 px-4" role="button" target="_blank" href="https://wa.link/vz9brx">Whatsapp</a></div>
                                    </div>
                                </div>
                            </section>
                        @endif
                    </div>
                </div>
            </div>
    </section>
@endsection
