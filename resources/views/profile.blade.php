@extends('admin.layout')

@section('content')
    <div class="pagetitle">
        <h1>Profile</h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Home</a></li>
                <li class="breadcrumb-item">Users</li>
                <li class="breadcrumb-item active">Profile</li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section profile">
        <div class="row">
            <div class="col-xl-4">

                <div class="card">
                    <div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
                        @if($user->profile_image != null && File::exists(public_path($user->profile_image)))
                            <img src="{{ asset($user->profile_image) }}" alt="Profile" class="rounded-circle" style="height: 120px; width: 120px">
                        @else
                            <img src="https://ui-avatars.com/api/?name={{ $user->name }}" alt="Profile" class="rounded-circle" style="height: 120px; width: 120px">
                        @endif
                        <h2>{{ $user->name }}</h2>
                        <h3>{{ ucwords(strtolower($user->role)) }}</h3>
                    </div>
                </div>

            </div>

            <div class="col-xl-8">

                <div class="card">
                    <div class="card-body pt-3">
                        <!-- Bordered Tabs -->
                        <ul class="nav nav-tabs nav-tabs-bordered">

                            <li class="nav-item">
                                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button>
                            </li>

                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit Profile</button>
                            </li>

                            <li class="nav-item">
                                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-change-password">Change Password</button>
                            </li>

                        </ul>
                        <div class="tab-content pt-2">

                            <div class="tab-pane fade show active profile-overview" id="profile-overview">

                                <h5 class="card-title">Profile Details</h5>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label ">Full Name</div>
                                    <div class="col-lg-9 col-md-8">{{ $user->name }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Email</div>
                                    <div class="col-lg-9 col-md-8">{{ $user->email }}</div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-3 col-md-4 label">Role</div>
                                    <div class="col-lg-9 col-md-8">{{ ucwords(strtolower($user->role)) }}</div>
                                </div>

                            </div>

                            <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

                                <!-- Profile Edit Form -->
                                <form>
                                    <div class="row mb-3">
                                        <label for="profile_image" class="col-md-4 col-lg-3 col-form-label">Profile Image</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input type="file" name="profile_image" id="profile_image" class="form-control">
                                            <div id="image-preview" class="mt-3" style="max-width: 240px;height: 240px;">
                                                @if($user->profile_image != null && File::exists(public_path($user->profile_image)))
                                                    <img src="{{ asset($user->profile_image) }}" alt="Profile" class="img-fluid" style="height: 240px; width:240px; max-width: 240px">
                                                @else
                                                    <img src="https://ui-avatars.com/api/?name={{ $user->name }}" alt="Profile" class="img-fluid" style=" height: 240px; width:240px; max-width: 240px">
                                                @endif
                                            </div>

                                            <div class="pt-2">
                                                <a href="#" class="btn btn-primary btn-sm" title="Upload new profile image" id="upload-image"><i class="bi bi-upload"></i> Upload Image</a>
                                                <a href="#" class="btn btn-danger btn-sm" title="Remove my profile image" id="delete-image"><i class="bi bi-trash"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <form id="update-profile-data">
                                    @csrf
                                    <div class="row mb-3">
                                        <label for="name" class="col-md-4 col-lg-3 col-form-label">Full Name</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="name" type="text" class="form-control" id="name" value="{{ $user->name }}">
                                        </div>
                                    </div>


                                    <div class="row mb-3">
                                        <label for="email" class="col-md-4 col-lg-3 col-form-label">Email</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="email" type="email" class="form-control" id="email" value="{{ $user->email }}" disabled>
                                        </div>
                                    </div>

                                    @if(Auth::user()->role === 'ADMIN')
                                        <div class="row mb-3">
                                            <label for="role" class="col-md-4 col-lg-3 col-form-label">Role</label>
                                            <div class="col-md-8 col-lg-9">
                                                <select class="form-select" aria-label="Default select example" name="role" id="role">
                                                    @foreach($roles as $role)
                                                        <option value="{{ $role }}" {{ $role == $user->role ? 'selected' : '' }}>{{ $role }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endif

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">Save Changes</button>
                                    </div>
                                </form><!-- End Profile Edit Form -->

                            </div>

                            <div class="tab-pane fade pt-3" id="profile-change-password">
                                <!-- Change Password Form -->
                                <form id="change-password-form">
                                    @csrf

                                    <div class="row mb-3">
                                        <label for="currentPassword" class="col-md-4 col-lg-3 col-form-label">Current Password</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="currentPassword" type="password" class="form-control" id="currentPassword">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="newPassword" class="col-md-4 col-lg-3 col-form-label">New Password</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="newPassword" type="password" class="form-control" id="newPassword">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <label for="newPassword_confirmation" class="col-md-4 col-lg-3 col-form-label">Re-enter New Password</label>
                                        <div class="col-md-8 col-lg-9">
                                            <input name="newPassword_confirmation" type="password" class="form-control" id="newPassword_confirmation">
                                        </div>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">Change Password</button>
                                    </div>
                                </form><!-- End Change Password Form -->

                            </div>

                        </div><!-- End Bordered Tabs -->

                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection

@push('customjs')
    <script>
        let cropper;
        $('#profile_image').change(function() {
            let reader = new FileReader();
            reader.onload = function(event) {
                $('#image-preview').html('<img src="' + event.target.result + '">');
                cropper = new Cropper($('#image-preview img')[0], {
                    aspectRatio: 1,
                    viewMode: 1
                });
            }
            reader.readAsDataURL(this.files[0]);
        })
        $('#upload-image').click(function() {
            if (cropper == null) {
                return
            }
            let croppedData = cropper.getData();
            let canvas = cropper.getCroppedCanvas();
            canvas.toBlob(function(blob) {
                let formData = new FormData();
                formData.append('profile_image', blob);
                let url = '{{route('profile.update-profile-image', ['user' => '%%id%%'])}}'
                url = url.replace('%%id%%', {{$user->id}});
                $.ajax({
                    url: url,
                    method: 'POST',
                    data: formData,
                    headers : {
                        'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                    },
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        Swal.fire({
                            icon : 'success',
                            title : 'Success',
                            text : response.message,
                            timer : 3000,
                            timerProgressBar: true,
                            onAfterClose : function () {
                                location.reload();
                            }
                        })
                    },
                    error: function(xhr, status, error) {
                        Toast.fire({
                            icon : 'error',
                            title : error
                        })
                    }
                });
            });
        });

        $('#delete-image').on('click', function (e) {
            e.preventDefault()
            Swal.fire({
                icon : 'warning',
                title : 'Delete Profile Photo?',
                text : 'Are you sure to delete the profile photo?',
                confirmButtonText: 'Yes',
                showCancelButton: true,
                confirmButtonColor: '#dc3545'
            }).then( (result) => {
                if (result.isConfirmed) {
                    let url = '{{ route('profile.delete-profile-image', ['user' => '%%id%%']) }}';
                    url = url.replace('%%id%%', {{ $user->id }});

                    $.ajax({
                        url : url,
                        type : 'POST',
                        headers : {
                            'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                        },
                        success: function(response) {
                            Swal.fire({
                                icon : 'success',
                                title : 'Success',
                                text : response.message,
                                timer : 3000,
                                timerProgressBar: true,
                                onAfterClose : function () {
                                    location.reload();
                                }
                            })
                        },
                        error: function(xhr, status, error) {
                            Toast.fire({
                                icon : 'error',
                                title : error
                            })
                        }
                    })
                }
            })
        })

        $('#update-profile-data').on('submit', function (e) {
            e.preventDefault()

            let url = '{{ route('profile.update-profile-data', ['user' => '%%id%%']) }}'
            url = url.replace('%%id%%', {{ $user->id }})

            $.ajax({
                url : url,
                type : 'POST',
                data : new FormData(this),
                processData : false,
                contentType : false,
                header : {
                    'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                },
                success: function(response) {
                    Swal.fire({
                        icon : 'success',
                        title : 'Success',
                        text : response.message,
                        timer : 3000,
                        timerProgressBar: true,
                        onAfterClose : function () {
                            location.reload();
                        }
                    })
                },
                error: function(xhr, status, error) {
                    if (xhr.status === 422) {
                        let errors = xhr.responseJSON.errors;
                        $('#update-profile-data :input').each(function () {
                            $(this).parent().children('div.invalid-feedback').remove();
                            $(this).removeClass('is-invalid');
                            if (errors.hasOwnProperty($(this).attr('name'))) {
                                let errorMessage = document.createElement('div');
                                errorMessage.classList.add('invalid-feedback');
                                errorMessage.innerHTML = errors[$(this).attr('name')];
                                $(this).parent().append(errorMessage);
                                $(this).addClass('is-invalid');
                            }
                        })
                    }
                    Toast.fire({
                        icon : 'error',
                        title : error
                    })
                }
            })

        })

        $('#change-password-form').on('submit', function (e) {
            e.preventDefault()

            let url = '{{ route('profile.change-password', ['user' =>'%%id%%']) }}'
            url = url.replace('%%id%%', {{ $user->id }})

            $.ajax({
                url : url,
                type : 'POST',
                data : new FormData(this),
                processData : false,
                contentType : false,
                headers : {
                    'X-CSRF-TOKEN' : '{{ csrf_token() }}'
                },
                success: function(response) {
                    Swal.fire({
                        icon : 'success',
                        title : 'Success',
                        text : response.message,
                        timer : 3000,
                        timerProgressBar: true,
                        onAfterClose : function () {
                            $('#change-password-form').trigger('reset')
                            $('#change-password-form :input').each(function () {
                                $(this).parent().children('div.invalid-feedback').remove();
                                $(this).removeClass('is-invalid');
                            })
                        }
                    })
                },
                error: function(xhr, status, error) {
                    if (xhr.status === 422) {
                        let errors = xhr.responseJSON.errors;
                        $('#change-password-form :input').each(function () {
                            $(this).parent().children('div.invalid-feedback').remove();
                            $(this).removeClass('is-invalid');
                            if (errors.hasOwnProperty($(this).attr('name'))) {
                                let errorMessage = document.createElement('div');
                                errorMessage.classList.add('invalid-feedback');
                                errorMessage.innerHTML = errors[$(this).attr('name')];
                                $(this).parent().append(errorMessage);
                                $(this).addClass('is-invalid');
                            }
                        })
                    }
                    Toast.fire({
                        icon : 'error',
                        title : error
                    })
                }
            })
        })
    </script>
@endpush
