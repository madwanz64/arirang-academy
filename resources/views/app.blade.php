<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Arirang Academy 2023</title>
    <meta content="Program dan pelatihan kursus bahasa Korea Arirang merupakan program kursus dan pelatihan untuk menghasilkan lulusan yang terampil bahasa Korea baik lisan maupun tulisan untuk berbagai tujuan dan berbagai konteks yang sesuai dengan kompetensi masing-masing level. Dan menjadi sarana agar dapat bekerja di Korea, bekerja di perusahaan Korea yang ada di Indonesia." name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ asset('assets/img/favicon.png') }}" rel="icon">
    <link href="{{ asset('assets/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/bootstrap-icons/bootstrap-icons.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/aos/aos.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/glightbox/css/glightbox.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendor/swiper/swiper-bundle.min.css') }}" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">
</head>
<body>
<!-- ======= Header ======= -->
<header id="header" class="header fixed-top d-flex align-items-center">
    <div class="container d-flex align-items-center justify-content-between">

        <a href="{{ route('home') }}" class="logo d-flex align-items-center me-auto me-lg-0">
            <h1>Arirang <span>Academy</span></h1>
        </a>

        <nav id="navbar" class="navbar flex-row justify-content-between">
            <ul>
                <li><a href="{{ Route::currentRouteName() !== 'home' ? route('home') : '' }}#home">Home</a></li>
                <li><a href="{{ Route::currentRouteName() !== 'home' ? route('home') : '' }}#greeting">Greeting</a></li>
                <li><a href="{{ Route::currentRouteName() !== 'home' ? route('home') : '' }}#about">About Us</a></li>
                <li><a href="{{ Route::currentRouteName() !== 'home' ? route('home') : '' }}#picture">Picture</a></li>
                <li><a href="{{ Route::currentRouteName() !== 'home' ? route('home') : '' }}#pengajar">Pengajar</a></li>
                <li><a href="{{ Route::currentRouteName() !== 'home' ? route('home') : '' }}#program">Program</a></li>
                <li><a href="{{ Route::currentRouteName() !== 'home' ? route('home') : '' }}#contact">Contact</a></li>
                @auth()
                    @if(Auth::user()->role == 'ADMIN' || Auth::user()->role == 'PENULIS')
                        <li><a href="{{ route('admin.dashboard') }}" class="btn text-bg-dark bg-gradient py-2 px-3">Dashboard</a></li>
                    @elseif(Auth::user()->role = 'MEMBER')
                        <li><a href="{{ route('profile.show', ['user' => Auth::id()]) }}" class="btn text-bg-dark bg-gradient py-2 px-3">Profile</a></li>
                    @endif
                @endauth
                @guest()
                    <li><a href="{{ route('login') }}" class="btn text-bg-dark bg-gradient py-2 px-3">Login</a></li>
                @endguest
{{--                <li><a href="register.html" target="_blank" class="btn text-bg-primary bg-gradient py-2 px-3">Register</a></li>--}}

            </ul>
        </nav>


        <subnav id="subnav" class="subnav">
            <div id="sub-navigation-bar" class="card fixed-top" style="margin-top: 75px;">
                <div class="card-header"
                     style="color: var(--bs-card-bg);border-radius: 0;background: var(--bs-card-bg);">
                    <ul class="nav flex-row justify-content-center">
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle nav-link" aria-expanded="true" data-bs-toggle="dropdown" href="#">Bahasa Korea</a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'video-kelas-bahasa-korea']) }}">Video kelas Bahasa Korea</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'idiom-korea']) }}">Idiom Korea</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'tata-bahasa']) }}">Tata Bahasa</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle nav-link" aria-expanded="true" data-bs-toggle="dropdown" href="#">Kuliah di Korea</a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'perkuliahan']) }}">Perkuliahan&nbsp;</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'universitas-di-korea']) }}">Univeristas di Korea</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'beasiswa']) }}">Beasiswa</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle nav-link" aria-expanded="true" data-bs-toggle="dropdown" href="#">Hallyu</a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'k-pop-k-drama']) }}">K-POP &amp; K-Drama</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'makanan-korea']) }}">Makanan Korea</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'budaya-tradisional']) }}">Budaya Tradisional</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'travelling']) }}">Travelling</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle nav-link" aria-expanded="true" data-bs-toggle="dropdown" href="#">Koresponden</a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'pengumuman']) }}">Pengumuman</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'qna-sharing']) }}">QnA Sharing</a></li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="dropdown-toggle nav-link" aria-expanded="true" data-bs-toggle="dropdown" href="#">Partisipasi</a>
                            <ul class="dropdown-menu" data-bs-popper="none">
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'berita-terkini']) }}">Berita Terkini</a></li>
                                <li><a class="dropdown-item" href="{{ route('article.index', ['slug' => 'kumpulan-cerita']) }}">Kumpulan Cerita</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </subnav>

        <i class="mobile-nav-toggle mobile-nav-show bi bi-list"></i>
        <i class="mobile-nav-toggle mobile-nav-hide d-none bi bi-x"></i>

    </div>
</header><!-- End Header -->

@yield('banner')

<main id="main">
    @yield('content')

    <footer class="text-center bg-dark">
        <div class="container text-white py-4 py-lg-5">
            <p class="text-muted mb-0">Copyright © Arirang Academy 2023</p>
        </div>
    </footer>

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<!-- End Footer -->



<!-- <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
        class="bi bi-arrow-up-short"></i></a>
<div id="preloader"></div> -->

<a href="https://wa.link/vz9brx" class="float-wa" target="_blank">
    <i class="bi bi-whatsapp"></i>
</a>
<script src="https://code.jquery.com/jquery-3.7.0.min.js"
        integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g="
        crossorigin="anonymous"></script>
<!-- Vendor JS Files -->


<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/aos/aos.js') }}"></script>
<script src="{{ asset('assets/vendor/glightbox/js/glightbox.min.js') }}"></script>
<script src="{{ asset('assets/vendor/purecounter/purecounter_vanilla.js') }}"></script>
<script src="{{ asset('assets/vendor/swiper/swiper-bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/php-email-form/validate.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('assets/js/main.js') }}"></script>


<script>
    function resizeMargin () {
        let reference = document.querySelector('#sub-navigation-bar')
        let referenceHeight = reference.offsetHeight;

        let body = document.querySelector('main#main')
        let marginTopValue = referenceHeight + "px";
        body.style.marginTop = marginTopValue
    }

    window.addEventListener('DOMContentLoaded', resizeMargin);
    window.addEventListener('resize', resizeMargin)
</script>
</body>
</html>
