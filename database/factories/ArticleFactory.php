<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Article>
 */
class ArticleFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Article::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake()->sentence(),
            'content' => htmlspecialchars('<p>').join(htmlspecialchars('</p><p>'), fake()->paragraphs(9)).htmlspecialchars('</p>'),
            'is_locked' => fake()->boolean() ? 1 : 0,
            'cover_image' => 'assets/img/2.png',
            'user_id' => 1
        ];
    }
}
