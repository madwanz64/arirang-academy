<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $bahasaKorea = Category::create([
            'name' => 'Bahasa Korea',
            'slug' => 'bahasa-korea',
        ]);

        $kuliahDiKorea = Category::create([
            'name' => 'Kuliah di Korea',
            'slug' => 'kuliah-di-korea',
        ]);

        $hallyu = Category::create([
            'name' => 'Hallyu',
            'slug' => 'hallyu',
        ]);

        $koresponden = Category::create([
            'name' => 'Koresponden',
            'slug' => 'koresponden',
        ]);

        $partisipasi = Category::create([
            'name' => 'Partisipasi',
            'slug' => 'partisipasi',
        ]);

        Category::create([
            'name' => 'Video Kelas Bahasa Korea',
            'slug' => 'video-kelas-bahasa-korea',
            'category_parent_id' => $bahasaKorea->id
        ]);

        Category::create([
            'name' => 'Idiom Korea',
            'slug' => 'idiom-korea',
            'category_parent_id' => $bahasaKorea->id
        ]);

        Category::create([
            'name' => 'Tata Bahasa',
            'slug' => 'tata-bahasa',
            'category_parent_id' => $bahasaKorea->id
        ]);

        Category::create([
            'name' => 'Perkuliahan',
            'slug' => 'perkuliahan',
            'category_parent_id' => $kuliahDiKorea->id
        ]);

        Category::create([
            'name' => 'Universitas di Korea',
            'slug' => 'universitas-di-korea',
            'category_parent_id' => $kuliahDiKorea->id
        ]);

        Category::create([
            'name' => 'Beasiswa',
            'slug' => 'beasiswa',
            'category_parent_id' => $kuliahDiKorea->id
        ]);

        Category::create([
            'name' => 'K-POP & K-Drama',
            'slug' => 'k-pop-k-drama',
            'category_parent_id' => $hallyu->id
        ]);

        Category::create([
            'name' => 'Makanan Korea',
            'slug' => 'makanan-korea',
            'category_parent_id' => $hallyu->id
        ]);

        Category::create([
            'name' => 'Budaya Tradisional',
            'slug' => 'budaya-tradisional',
            'category_parent_id' => $hallyu->id
        ]);

        Category::create([
            'name' => 'Travelling',
            'slug' => 'travelling',
            'category_parent_id' => $hallyu->id
        ]);

        Category::create([
            'name' => 'Pengumuman',
            'slug' => 'pengumuman',
            'category_parent_id' => $koresponden->id
        ]);

        Category::create([
            'name' => 'QnA Sharing',
            'slug' => 'qna-sharing',
            'category_parent_id' => $koresponden->id
        ]);

        Category::create([
            'name' => 'Berita Terkini',
            'slug' => 'berita-terkini',
            'category_parent_id' => $partisipasi->id
        ]);

        Category::create([
            'name' => 'Kumpulan Cerita',
            'slug' => 'kumpulan-cerita',
            'category_parent_id' => $partisipasi->id
        ]);
    }
}
