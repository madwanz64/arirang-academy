<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        for ($i = 6; $i <= 19; $i++) {
            $number = rand(7, 18);
            for ($j = 0; $j < $number; $j++) {
                Article::factory()->create([
                    'category_id' => $i
                ]);
            }
        }
    }
}
