<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Article extends Model
{
    use HasFactory;

    protected $fillable = [
        'category_id',
        'cover_image',
        'title',
        'content',
        'is_locked',
        'user_id'
    ];

    public function author() : BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
