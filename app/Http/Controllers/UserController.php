<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index() {
        return view('admin.user.index');
    }

    public function getUsers() : JsonResponse
    {
        $draw = request()->input('draw');
        $start = request()->input('start');
        $length = request()->input('length');
        $searchValue = request()->input('search.value');
        $orderColumn = request()->input('order.0.column');
        $orderDir = request()->input('order.0.dir');

        $columns = [
            0 => 'name',
            1 => 'email',
            2 => 'role',
            // add more columns here...
        ];


        $query = User::query()
            ->select(['id', 'name', 'email', 'role']);

        if (!empty($searchValue)) {
            $query->where(function ($q) use ($searchValue) {
                $q->where('name', 'LIKE', "%$searchValue%")
                    ->orWhere('email', 'LIKE', "%$searchValue%")
                    ->orWhere('role', 'LIKE', "%$searchValue%");
            });
        }

        if ($orderColumn != null) {
            $query->orderBy($columns[$orderColumn], $orderDir);
        }

        $totalCount = $query->count();
        $query->skip($start)->take($length);
        $data = $query->get();

        return response()->json([
            'draw' => $draw,
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $totalCount,
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $roles = ['ADMIN', 'PENULIS', 'MEMBER'];

        return view('admin.user.create', [
            'roles' => $roles
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) : JsonResponse
    {
        $validated = $request->validate([
            'name' => ['required'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'min:8', 'string'],
            'role' => ['required', Rule::in(['ADMIN', 'MEMBER', 'PENULIS'])]
        ]);

        $user = new User();
        $user->name = $validated['name'];
        $user->email = $validated['email'];
        $user->password = Hash::make($validated['password']);
        $user->role = $validated['role'];
        $user->save();

        return response()->json([
            'message' => 'Success! The data has been created successfully'
        ]);
    }
}
