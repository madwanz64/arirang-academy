<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show($slug)
    {
        $category = Category::where('slug', $slug)->first();

        if ($category == null) {
            abort(404);
        }

        $articles = Article::where('category_id', $category->id)->with('author')->paginate(6);

        for ($i = 0; $i < count($articles); $i++) {
            $articles[$i]['content'] = htmlspecialchars_decode($articles[$i]['content']);

            $count = 0;
            $index = 0;

            while ($count < 1) {
                $index = strpos($articles[$i]['content'], "</p>", $index + 1);
                if ($index === false) {
                    break;
                }
                $count++;
            }

            if ($count === 1) {
                $articles[$i]['content'] = substr($articles[$i]['content'], 0, $index + 4); // Include "</p>" in the final string
            }
        }

        return view('article.index', [
            'category' => $category,
            'articles' => $articles
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Category $category)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        //
    }
}
