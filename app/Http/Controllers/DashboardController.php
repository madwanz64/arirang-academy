<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
        $articlesCount = Article::query()->count();
        $authorCount = User::query()->where('role', '=', 'PENULIS')->orWhere('role', '=', 'ADMIN')->count();
        $memberCount = User::query()->where('role', '=', 'MEMBER')->count();

        $recentArticles = Article::query()
            ->orderBy('created_at', 'desc')
            ->limit(5)->get();

        return view('admin.dashboard', [
            'articlesCount' => $articlesCount,
            'authorCount' => $authorCount,
            'memberCount' => $memberCount,
            'recentArticles' => $recentArticles
        ]);
    }
}
