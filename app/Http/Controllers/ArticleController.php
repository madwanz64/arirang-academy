<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\Category;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('admin.article.index');
    }

    public function getArticles() : JsonResponse
    {
        $draw = request()->input('draw');
        $start = request()->input('start');
        $length = request()->input('length');
        $searchValue = request()->input('search.value');
        $orderColumn = request()->input('order.0.column');
        $orderDir = request()->input('order.0.dir');

        $columns = [
            0 => 'articles.title',
            1 => 'categories.name',
            2 => 'articles.is_locked',
            3 => 'users.name'
            // add more columns here...
        ];


        $query = Article::query()
            ->leftJoin('categories', 'categories.id', '=', 'articles.category_id')
            ->leftJoin('users', 'users.id', '=', 'articles.user_id')
            ->select(['articles.id', 'articles.title', 'articles.is_locked', 'categories.name', \DB::raw('users.name AS author')]);

        if (!empty($searchValue)) {
            $query->where(function ($q) use ($searchValue) {
                $q->where('articles.title', 'LIKE', "%$searchValue%")
                    ->orWhere('categories.name', 'LIKE', "%$searchValue%")
                    ->orWhere('users.name', 'LIKE', "%$searchValue%");
            });
        }

        if ($orderColumn != null) {
            $query->orderBy($columns[$orderColumn], $orderDir);
        }

        $totalCount = $query->count();
        $query->skip($start)->take($length);
        $data = $query->get();

        return response()->json([
            'draw' => $draw,
            'recordsTotal' => $totalCount,
            'recordsFiltered' => $totalCount,
            'data' => $data
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $categories = Category::where('category_parent_id', null)->get();

        foreach ($categories as $category) {
            $category->children = Category::where('category_parent_id', $category->id)->get();
        }

        return view('admin.article.create', [
            'categories' => $categories
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) : JsonResponse
    {
        $validated = $request->validate([
            'category_id' => ['required', 'exists:categories,id'],
            'cover_image' => ['sometimes', 'image'],
            'title' => ['required'],
            'content' => ['required'],
            'is_locked' => ['required']
        ]);

        $url = null;

        if ($request->hasFile('cover_image')) {
            $file = $request->file('cover_image');
            $extension = $file->getClientOriginalExtension();
            $filename = uniqid('cover-image').'.'.$extension;
            $file->move(public_path('attachments/cover-images'), $filename);
            $url = 'attachments/cover-images/'.$filename;
        }

        $article = new Article();
        $article->category_id = $validated['category_id'];
        $article->cover_image = $url;
        $article->title = $validated['title'];
        $article->content = htmlspecialchars($validated['content']);
        $article->is_locked = $validated['is_locked'];
        $article->user_id = \Auth::id();
        $article->save();

        return response()->json([
            'message' => 'Success! The data has been created successfully'
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(Article $article)
    {
        $user = User::find($article->user_id)->select(['name', 'email'])->first();
        $article->user = $user;
        $article->content = htmlspecialchars_decode($article->content);

        if ($article->is_locked) {
            $count = 0;
            $index = 0;

            while ($count < 3) {
                $index = strpos($article->content, "</p>", $index + 1);
                if ($index === false) {
                    break;
                }
                $count++;
            }

            if ($count === 3) {
                $article->content = substr($article->content, 0, $index + 4); // Include "</p>" in the final string
            }
        }

        return view('article.show', [
            'article' => $article
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Article $article)
    {
        $categories = Category::where('category_parent_id', null)->get();

        foreach ($categories as $category) {
            $category->children = Category::where('category_parent_id', $category->id)->get();
        }

        return view('admin.article.edit', [
            'article' => $article,
            'categories' => $categories
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Article $article) : JsonResponse
    {
        $validated = $request->validate([
            'category_id' => ['required', 'exists:categories,id'],
            'cover_image' => ['sometimes', 'image'],
            'title' => ['required'],
            'content' => ['required'],
            'is_locked' => ['required']
        ]);

        $url = null;

        if ($request->hasFile('cover_image')) {
            $file = $request->file('cover_image');
            $extension = $file->getClientOriginalExtension();
            $filename = uniqid('cover-image').'.'.$extension;
            $file->move(public_path('attachments/cover-images'), $filename);
            $url = 'attachments/cover-images/'.$filename;

            if ($article->cover_image != null && File::exists(public_path($article->cover_image))) {
                File::delete(public_path($article->cover_image));
            }

            $article->cover_image = $url;
        }

        $article->category_id = $validated['category_id'];
        $article->title = $validated['title'];
        $article->content = htmlspecialchars($validated['content']);
        $article->is_locked = $validated['is_locked'];
        $article->save();

        return response()->json([
            'message' => 'Success! The data has been updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Article $article)
    {
        if ($article->cover_image != null && File::exists(public_path($article->cover_image))) {
            File::delete(public_path($article->cover_image));
        }

        $article->delete();

        return response()->json([
            'message' => "Success! The data has been deleted successfully"
        ]);
    }
}
