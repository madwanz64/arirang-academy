<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;
use Intervention\Image\Facades\Image;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        $roles = ['ADMIN', 'PENULIS', 'MEMBER'];

        return view('profile', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function updateProfileData(Request $request, User $user) : JsonResponse
    {
        $validated = $request->validate([
            'name' => ['required'],
            'role' => [Rule::excludeIf(\Auth::user()->role != 'ADMIN'), Rule::in(['ADMIN', 'PENULIS', 'MEMBER'])]
        ]);

        $user->name = $validated['name'];
        if (\Auth::user()->role == "ADMIN") {
            $user->role = $validated['role'];
        }
        $user->save();

        return response()->json([
            'message' => "Success! The data has been updated successfully"
        ]);
    }

    public function updateProfileImage(Request $request, User $user) : JsonResponse
    {
        $validated = $request->validate([
            'profile_image' => ['required', 'image']
        ]);

        if ($request->hasFile('profile_image')) {
            $file = $request->file('profile_image');
            $image = Image::make($file);
            $image->fit(300, 300);
            $filename = uniqid('profil-image').'.'.'jpg';
            $url = 'images/profile-images/'.$filename;
            if (!is_dir('images/profile-images/')) {
                mkdir('images/profile-images/', 0777, true);
            }
            $image->save($url);

            if ($user->profile_image != null && File::exists(public_path($user->profile_image))) {
                File::delete(public_path($user->profile_image));
            }

            $user->profile_image = $url;
            $user->save();
        }


        return response()->json([
            'message' => "Success! The data has been updated successfully"
        ]);
    }

    public function deleteProfileImage(Request $request, User $user) : JsonResponse
    {
        if ($user->profile_image != null && File::exists(public_path($user->profile_image))) {
            File::delete(public_path($user->profile_image));

            $user->profile_image = null;
        } else {
            return response()->json([
                'message' => 'There is no profile picture to be deleted'
            ], 422);
        }

        return response()->json([
            'message' => "Success! The data has been deleted successfully"
        ]);
    }

    public function changePassword (Request $request, User $user) : JsonResponse
    {
        $validated = $request->validate([
            'currentPassword' => ['required'],
            'newPassword' => ['required', 'min:8', 'confirmed']
        ]);

        if (!Hash::check($validated['currentPassword'], $user->password)) {
            throw ValidationException::withMessages([
                'currentPassword' => 'The current password is incorrect.'
            ]);
        }

        $user->password = Hash::make($validated['newPassword']);
        $user->save();

        return response()->json([
            'message' => "Success! The data has been updated successfully"
        ]);
    }
}
