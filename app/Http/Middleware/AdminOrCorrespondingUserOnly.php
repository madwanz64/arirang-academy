<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AdminOrCorrespondingUserOnly
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $authenticatedUser = \Auth::user();

        if ($authenticatedUser->role === 'ADMIN') {
            return $next($request);
        }

        $user = $request->route('user');

        if ($user && $user->id === $authenticatedUser->id) {
            return $next($request);
        }

        abort(403);
    }
}
